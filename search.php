<?php get_header(); ?>

	<div class="gdlr-content">

		<!-- Search section -->
		<div class="wrapper">
			<section id="content-section-1">
				<div class="section-container container">

					<div class="gldr-item gdlr-content-item" style="margin-bottom: 100px;">
		
						<h1 class="page-title">Search</h1>
						<?php get_search_form(); ?>
						
						<div id="search-results" class="entry-content ">
							<h2 class="search-title"> <?php echo $wp_query->found_posts; ?>
								<?php _e( 'Search Results Found For', 'locale' ); ?>: "<?php the_search_query(); ?>" 
							</h2>

							<?php if ( have_posts() ) { ?>

								<ul>

								<?php while ( have_posts() ) { the_post(); ?>

									<li>
										
									<?php  the_post_thumbnail('thumbnail') ?>
										<div class="search-item">
										<h3><a href="<?php echo get_permalink(); ?>">
											<?php the_title();  ?>
										</a></h3>
										<?php echo substr(get_the_excerpt(), 0,200); ?>. . .
										<div class="h-readmore"> <a href="<?php the_permalink(); ?>">Read More</a></div>
										</div>
									</li>

								<?php } ?>

								</ul>

								<?php echo paginate_links(); ?>

							<?php } ?>

								</div>
						
							</div>
						</section>
					</div><!-- wrapper -->
		
	</div><!-- gdlr-content -->
<?php get_footer(); ?>
