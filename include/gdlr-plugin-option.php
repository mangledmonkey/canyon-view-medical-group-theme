<?php
	/*	
	*	Goodlayers Framework File
	*	---------------------------------------------------------------------
	*	This file contains the code to register the goodlayers plugin option
	*	to admin area
	*	---------------------------------------------------------------------
	*/

	// add an admin option to menu
	add_filter('gdlr_admin_option', 'gdlr_register_menu_admin_option');
	if( !function_exists('gdlr_register_menu_admin_option') ){
		
		function gdlr_register_menu_admin_option( $array ){		
			global $gdlr_sidebar_controller;
		
			if( empty($array['general']['options']) ) return $array;
			
			$menu_option = array( 									
				'title' => __('Food Style', 'gdlr_translate'),
				'options' => array(
					'menu-slug' => array(
						'title' => __('Food Slug ( Permalink )', 'gdlr_translate'),
						'type' => 'text',
						'default' => 'menu',
						'description' => __('Only <strong>a-z (lower case), hyphen and underscore</strong> is allowed here <br><br>', 'gdlr_translate') .
							__('After changing, you have to set the permalink at the setting > permalink to default (to reset the permalink rules) as well.', 'gdlr_translate')
					),
					'menu-category-slug' => array(
						'title' => __('Food Category Slug ( Permalink )', 'gdlr_translate'),
						'type' => 'text',
						'default' => 'menu_category',
					),
					'menu-tag-slug' => array(
						'title' => __('Food Tag Slug ( Permalink )', 'gdlr_translate'),
						'type' => 'text',
						'default' => 'menu_tag',
					),				
					'menu-thumbnail-size' => array(
						'title' => __('Single Food Thumbnail Size', 'gdlr_translate'),
						'type'=> 'combobox',
						'options'=> gdlr_get_thumbnail_list(),
						'default'=> 'post-thumbnail-size'
					),	
					'enable-menu-link' => array(
						'title' => __('Enable Single Food Link', 'gdlr_translate'),
						'type'=> 'checkbox'
					),	
					'single-menu-author' => array(
						'title' => __('Enable Single Food Author', 'gdlr_translate'),
						'type'=> 'checkbox'
					),
					'single-menu-related' => array(
						'title' => __('Enable Related Food', 'gdlr_translate'),
						'type'=> 'checkbox'
					),	
					'related-menu-thumbnail-size' => array(
						'title' => __('Related Menu Thumbnail Size', 'gdlr_translate'),
						'type'=> 'combobox',
						'options'=> gdlr_get_thumbnail_list(),
						'default'=> 'post-thumbnail-size'
					),					
				)
			);
			
			$array['general']['options']['menu-style'] = $menu_option;
			
			$menu_option = array( 									
				'title' => __('Event Style', 'gdlr_translate'),
				'options' => array(
					'events-title' => array(
						'title' => __('Events Title', 'gdlr_translate'),
						'type'=> 'text',
						'default'=> __('Events', 'gdlr_translate')
					),
					'events-caption' => array(
						'title' => __('Events Caption', 'gdlr_translate'),
						'type'=> 'text'
					),
					'single-event-title' => array(
						'title' => __('Single Event Title', 'gdlr_translate'),
						'type'=> 'text',
						'default'=> __('Event', 'gdlr_translate')
					),
					'single-event-caption' => array(
						'title' => __('Single Event Caption', 'gdlr_translate'),
						'type'=> 'text'
					),					
				)
			);
			
			$array['general']['options']['event-style'] = $menu_option;
			
			return $array;
		}
		
	}		

?>