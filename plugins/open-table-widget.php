<?php
	/*	
	*	Goodlayers Open Table Widget Support File
	*/

	// add open table in page builder area
	add_filter('gdlr_page_builder_option', 'gdlr_register_opt_widget_item');
	if( !function_exists('gdlr_register_opt_widget_item') ){
		function gdlr_register_opt_widget_item( $page_builder = array() ){
			global $gdlr_spaces;
		
			$page_builder['content-item']['options']['open-table'] = array(
				'title'=> __('Open Table Form', 'gdlr-menu'), 
				'type'=>'item',
				'options'=>array_merge(gdlr_page_builder_title_option(true), array(	
					'restaurant-id'=> array(
						'title'=> __('Restaurant ID (From Open Table)' ,'gdlr-menu'),
						'type'=> 'text'
					),					
					'margin-bottom' => array(
						'title' => __('Margin Bottom', 'gdlr-menu'),
						'type' => 'text',
						'default' => $gdlr_spaces['bottom-blog-item'],
						'description' => __('Spaces after ending of this item', 'gdlr-menu')
					),				
				))
			);
			return $page_builder;
		}
	}
	
	// page item section
	add_action('gdlr_print_item_selector', 'gdlr_check_opt_widget_item', 10, 2);
	if( !function_exists('gdlr_check_opt_widget_item') ){
		function gdlr_check_opt_widget_item( $type, $settings = array() ){ 
			if($type != 'open-table'){ return ''; }

			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $gdlr_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $gdlr_spaces['bottom-blog-item'])? 'margin-bottom: ' . $settings['margin-bottom'] . ';': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			echo '<div class="open-table-item-wrapper"  ' . $item_id . $margin_style . '>';
			echo gdlr_get_item_title($settings);
?>
<form method="get" class="gdlr-otw-item-form" action="http://www.opentable.com/restaurant-search.aspx" target="_blank">
	<div class="otw-wrapper">
		<div class="gdlr-otw-item-column">
			<div class="otw-date-li otw-input-wrap">
				<i class="fa fa-calendar-o"></i>
				<div class="gdlr-otw-item-inner">
					<input name="startDate" placeholder="<?php _e('Date', 'gdlr_translate'); ?>" class="otw-reservation-date" type="text" value="" autocomplete="off">
				</div>
			</div>
		</div>
		<div class="gdlr-otw-item-column">
			<div class="otw-time-wrap otw-input-wrap">
				<i class="fa fa-clock-o"></i>
				<div class="gdlr-otw-item-inner gdlr-otw-combobox">
					<select name="ResTime" class="otw-reservation-time">
						<?php
						//Time Loop
						//@SEE: http://stackoverflow.com/questions/6530836/php-time-loop-time-one-and-half-of-hour
						$inc = 30 * 60;
						$start = ( strtotime( '5PM' ) ); // 6  AM
						$end = ( strtotime( '11:59PM' ) ); // 10 PM

						for ( $i = $start; $i <= $end; $i += $inc ) {
							// to the standart format
							$time      = date( 'g:i a', $i );
							$timeValue = date( 'g:ia', $i );
							$default   = "7:00pm";
							echo "<option value=\"$timeValue\" " . ( ( $timeValue == $default ) ? ' selected="selected" ' : "" ) . ">$time</option>" . PHP_EOL;
						}
						?>
					</select>
				</div>
			</div>
		</div>
		<div class="gdlr-otw-item-column">
			<div class="otw-party-size-wrap otw-input-wrap">
				<i class="fa fa-users"></i>
				<div class="gdlr-otw-item-inner gdlr-otw-combobox">
					<select name="partySize" class="otw-party-size-select">
						<option value="1">1 Person</option>
						<option value="2" selected="selected">2 People</option>
						<option value="3">3 People</option>
						<option value="4">4 People</option>
						<option value="5">5 People</option>
						<option value="6">6 People</option>
						<option value="7">7 People</option>
						<option value="8">8 People</option>
						<option value="9">9 People</option>
						<option value="10">10 People</option>
					</select>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="otw-button-wrap">
			<input type="submit" class="otw-submit-btn" value="<?php _e( 'Find a Table', 'open-table-widget' ); ?>" />
		</div>
		<input type="hidden" name="RestaurantID" class="RestaurantID" value="<?php echo esc_attr($settings['restaurant-id']); ?>">
		<input type="hidden" name="rid" class="rid" value="<?php echo esc_attr($settings['restaurant-id']); ?>">
		<input type="hidden" name="GeoID" class="GeoID" value="15">
		<input type="hidden" name="txtDateFormat" class="txtDateFormat" value="MM/dd/yyyy">
		<input type="hidden" name="RestaurantReferralID" class="RestaurantReferralID" value="<?php echo esc_attr($settings['restaurant-id']); ?>">
	</div>
</form>
<?php
			echo '</div>';
		}
	}	