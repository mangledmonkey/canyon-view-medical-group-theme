<?php
	/*	
	*	Goodlayers Event Calendar Support File
	*/

	// add open event item to page builder area
	add_filter('gdlr_page_builder_option', 'gdlr_register_event_item');
	if( !function_exists('gdlr_register_event_item') ){
		function gdlr_register_event_item( $page_builder = array() ){
			global $gdlr_spaces;
		
			$page_builder['content-item']['options']['event'] = array(
				'title'=> __('Event', 'gdlr-menu'), 
				'type'=>'item',
				'options'=>array_merge(gdlr_page_builder_title_option(true), array(	
					'category'=> array(
						'title'=> __('Category' ,'gdlr-menu'),
						'type'=> 'multi-combobox',
						'options'=> gdlr_get_term_list('tribe_events_cat'),
						'description'=> __('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'gdlr-menu')
					),	
					'tag'=> array(
						'title'=> __('Tag' ,'gdlr-menu'),
						'type'=> 'multi-combobox',
						'options'=> gdlr_get_term_list('post_tag'),
						'description'=> __('Will be ignored when the menu filter option is enabled.', 'gdlr-menu')
					),	
					'num-fetch'=> array(
						'title'=> __('Num Fetch' ,'gdlr_translate'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> __('Specify the number of posts you want to pull out.', 'gdlr_translate')
					),
					'column'=> array(
						'title'=> __('Menu Column' ,'gdlr-menu'),
						'type'=> 'combobox',
						'options'=> array(
							'4'=>'4',
							'3'=>'3',
							'2'=>'2',
							'1'=>'1'
						),
						'default'=>'3'
					),			
					'orderby'=> array(
						'title'=> __('Order By' ,'gdlr-menu'),
						'type'=> 'combobox',
						'options'=> array(
							'date' => __('Publish Date', 'gdlr-menu'), 
							'title' => __('Title', 'gdlr-menu'), 
							'rand' => __('Random', 'gdlr-menu'), 
						)
					),
					'order'=> array(
						'title'=> __('Order' ,'gdlr-menu'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>__('Descending Order', 'gdlr-menu'), 
							'asc'=> __('Ascending Order', 'gdlr-menu'), 
						)
					),			
					'pagination'=> array(
						'title'=> __('Enable Pagination' ,'gdlr-menu'),
						'type'=> 'checkbox'
					),								
					'margin-bottom' => array(
						'title' => __('Margin Bottom', 'gdlr-menu'),
						'type' => 'text',
						'default' => $gdlr_spaces['bottom-blog-item'],
						'description' => __('Spaces after ending of this item', 'gdlr-menu')
					),				
				))
			);
			return $page_builder;
		}
	}
	
	// page item section
	add_action('gdlr_print_item_selector', 'gdlr_check_event_item', 10, 2);
	if( !function_exists('gdlr_check_event_item') ){
		function gdlr_check_event_item( $type, $settings = array() ){ 
			if($type != 'event'){ return ''; }

			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $gdlr_spaces, $post;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $gdlr_spaces['bottom-blog-item'])? 'margin-bottom: ' . $settings['margin-bottom'] . ';': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			echo '<div class="gdlr-event-item-wrapper"  ' . $item_id . $margin_style . '>';
			echo gdlr_get_item_title($settings);

			$args = array('post_type' => 'tribe_events', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : get_query_var('page');
			$args['paged'] = empty($args['paged'])? 1: $args['paged'];
			
			if( !empty($settings['category']) || !empty($settings['tag']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'tribe_events_cat', 'field'=>'slug'));
				}
				if( !empty($settings['tag']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'post_tag', 'field'=>'slug'));
				}				
			}
			
			$query = new WP_Query( $args );
			
			echo '<div class="gdlr-event-item-holder">';
			$current_size = 0;
			$size = intval($settings['column']);
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					echo '<div class="clear"></div>';
				}
				
				echo '<div class="' . gdlr_get_column_class('1/' . $size) . '">';
				echo '<div class="gdlr-item gdlr-event-list">';
				echo '<div class="gdlr-event-list-date">';
				echo '<span class="gdlr-date-day">' . tribe_get_start_date($post, false, $format = 'd' ) . '</span>';
				echo '<span class="gdlr-date-divider"></span>';
				echo '<span class="gdlr-date-month">' . tribe_get_start_date($post, false, $format = 'M' ) . '</span>';
				echo '</div>';
				
				echo '<div class="gdlr-event-list-content gdlr-skin-box">';
				echo '<h3 class="gdlr-event-list-title gdlr-content-font gdlr-skin-content"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>';
				echo '<div class="gdlr-event-list-date-info .gdlr-skin-info"><i class="fa fa-clock-o" ></i>';
				echo tribe_get_start_date($post, false, $format = 'h:m a' ) . ' - ' . tribe_get_end_date($post, false, $format = 'h:m a' );
				echo '</div>';
				echo '</div>'; // gdlr-event-list-content
				echo '</div>'; // gdlr-event-list
				echo '</div>'; // column_class
				$current_size ++;
			}
			echo '<div class="clear"></div>';
			echo '</div>'; // event-item-holder
			
			if( $settings['pagination'] == 'enable' ){
				$ret .= gdlr_get_pagination($query->max_num_pages, $args['paged']);
			}
			echo '</div>'; // event-item-wrapper
		}
	}	