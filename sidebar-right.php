<?php
/**
 * A template for calling the right sidebar in everypage
 */
 
	global $gdlr_sidebar, $gdlr_post_option;
?>

<?php if( $gdlr_sidebar['type'] == 'right-sidebar' || $gdlr_sidebar['type'] == 'both-sidebar' ){ ?>
<div class="gdlr-sidebar gdlr-right-sidebar <?php echo esc_attr($gdlr_sidebar['right']); ?> columns">
	<div class="gdlr-item-start-content sidebar-right-item" >
	<?php 
		if( is_single() && get_post_type() == 'menu' ){
			echo '<div class="gdlr-menu-sidebar gdlr-item" >';
			echo gdlr_get_single_menu_head($gdlr_post_option);
			echo gdlr_get_menu_info('ingredients', $gdlr_post_option);
			echo '</div>';
		}
	
	
		$sidebar_id = gdlr_get_sidebar_id($gdlr_sidebar['right-sidebar']);
		if( is_active_sidebar($sidebar_id) ){ 
			dynamic_sidebar($sidebar_id); 
		}
	?>
	</div>
</div>
<?php } ?>